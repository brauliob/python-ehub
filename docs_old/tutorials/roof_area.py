"""
This is a file for a tutorial on how to add roof constraints to a model.

To run this file, from the root directory of this project, do

    python3.6 -m docs.tutorials.roof_area
"""
import os

from outputter import pretty_print
from energy_hub import EHubModel
from energy_hub.utils import constraint

MAX_SOLAR_AREA = 500


class RoofModel(EHubModel):
    """
    This is a subclass of EHubModel with roof constraints.
    """

    @constraint()
    def roof_tech_area_below_max(self):
        """ Ensure the roof techs are taking up less area than there is roof.
        """
        roof_tech = self._data.roof_tech

        # Here, capacities for roof techs mean area
        total_roof_area = sum(self.capacities[roof] for roof in roof_tech)

        return total_roof_area <= MAX_SOLAR_AREA


def main():
    """
    This is the main executing function of the script.

    It is considered good practise to have a main function for a script.
    """
    # This is a cross-platform way of getting the path to the Excel file
    current_directory = os.path.dirname(os.path.realpath(__file__))
    excel_file = os.path.join(current_directory, 'extension.xlsx')

    # Here's where we instantiate our model. Nothing is solved at this point.
    my_model = RoofModel(excel=excel_file)

    # Now we solve the model and get back our results
    results = my_model.solve()

    # Now we print the results to the console so we can view them in a nice way
    pretty_print(results)


# If we are being run as a script
if __name__ == '__main__':
    main()
