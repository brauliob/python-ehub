"""
This is a file that shows how to add something to a constraint.
"""
import os

from pyehub.energy_hub.ehub_model import EHubModel
from pyehub.energy_hub.utils import constraint
from pyehub.outputter import pretty_print


class MyModel(EHubModel):
    """
    This is a subclass of the original EHubModel.

    We create a subclass so that we can modify the superclass (or parent
    class) without modifying the superclass's source code.
    """

    @constraint()
    def calc_operating_cost(self):
        """
        Calculates a new operating cost that uses a price per time step.

        Instead of using a fixed price for electricity from the grid, we
        use a time series that contains the price per time step.
        """
        total_export_income = 0

        for stream in self.export_streams:
            # If the price is time series
            if self.FEED_IN_TARIFFS[stream] in self.TIME_SERIES:
                for t in self.time:
                    price = self.TIME_SERIES[self.FEED_IN_TARIFFS[stream]][t]
                    energy_exported = self.energy_exported[t][stream]
                    total_export_income += price * energy_exported

            else:
                total_energy_exported = sum(self.energy_exported[time][stream]
                                            for time in self.time)
                price = self.FEED_IN_TARIFFS[stream]
                total_export_income += price * total_energy_exported

        grid_price = {0: 0.13,
                      1: 1.13,
                      2: 2.13,
                      3: 0.13,
                      4: 0.13,
                      5: 0.13,
                      6: 0.13,
                      7: 0.13,
                      8: 0.13,
                      9: 0.13,
                      10: 0.13}
        total_fuel_bill = 0
        for tech in self._data.converters:
            # assuming that any converter takes only single input(i.e. only Grid, only Gas, etc.) If not, we need to add
            if 'Grid' in tech.inputs:
                total_fuel_bill += sum(grid_price[t]
                                      * self.energy_imported[t]['Grid']
                                      for t in self.time)

            # Default to the fixed price for non `Grid` converters
            for inp in tech.inputs:
                if (inp != 'Grid') and (inp in self.import_streams):
                    total_fuel_bill += self.FUEL_PRICES[inp]*sum(self.energy_imported[t][inp] for t in self.time)

        return self.operating_cost == total_fuel_bill - total_export_income


def main():
    """The main function of this script."""
    # This is a cross-platform way of getting the path to the Excel file
    current_directory = os.path.dirname(os.path.realpath(__file__))
    excel_file = os.path.join(current_directory, 'test_file_all_constraints_work.xlsx')

    # Here's where we instantiate our model. Nothing is solved at this point.
    my_model = MyModel(excel=excel_file)

    # Now we solve the model and get back our results
    results = my_model.solve()

    # Now we print the results to the console so we can view them in a nice way
    pretty_print(results)


if __name__ == '__main__':
    main()
